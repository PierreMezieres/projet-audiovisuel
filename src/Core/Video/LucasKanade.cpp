//
// Created by pierre on 02/01/19.
//

#include <Log/Log.hpp>
#include "LucasKanade.hpp"
#include "Deriv.hpp"

LucasKanade::LucasKanade(Mat i, Mat ip, int mode) {
    I = i;
    Iprev = ip;
    m_mode = mode;
}

void LucasKanade::compute() {
    switch(m_mode){
        case 0: computeCV(); break;
        case 1: computeHM(); break;
        default: break;
    }
}

void LucasKanade::getRes(Points &p1, Points &p2) {
    p1 = interestPoints;
    p2 = resPoints;
}

void LucasKanade::computeCV() {

    cv::goodFeaturesToTrack(I, interestPoints, max_point, 0.01,10, Mat(), 3, 0, 0.04);
    cv::cornerSubPix(I, interestPoints, subPixWinSize, cv::Size(-1,-1), termCriteria);

    std::vector<uchar> status;
    std::vector<float> err;
    cv::calcOpticalFlowPyrLK(Iprev, I, interestPoints, resPoints, status, err, winSize, 3, termCriteria, 0, 0.001);

}




void LucasKanade::computePixel(int r, int c, double &x, double &y) {

    double s1=0,s2=0,s4=0,s5=0,s6=0;
    for(int i=-hw; i<=hw; i++){
        for(int j=-hw;j<=hw;j++){
            int R = i + r;
            int C = j + c;
            double ix = Ix.at<int>(R,C);
            double iy = Iy.at<int>(R,C);
            double it = It.at<int>(R,C);
            s1 = s1 + ix * ix;
            s2 = s2 + ix * iy;
            s4 = s4 + iy * iy;
            s5 = s5 + ix * it;
            s6 = s6 + iy * it;
        }
    }

    s5 = -s5;
    s6 = -s6;
    Matrix2 m(s1,s2,s2,s4);
    m=glm::inverse(m);

    x = m[0][0] * s5 + m[0][1] * s6;
    y = m[1][0] * s5 + m[1][1] * s6;

}

void LucasKanade::computeHM() {
    hw = 15;

    Deriv deriv(Iprev, I);
    Ix = deriv.getIx();
    Iy = deriv.getIy();
    It = deriv.getIt();
    int r = I.rows;
    int c = I.cols;


    Vx.create(r, c, CV_64F);
    Vy.create(r, c, CV_64F);
    Vxy.create(r, c, CV_8U);
    Vx.setTo(0);
    Vy.setTo(0);
    Vxy.setTo(0);


    Mat test;
    test.create(r,c,CV_8U);
    test.setTo(0);

    cv::goodFeaturesToTrack(I, interestPoints, max_point, 0.01,10, Mat(), 3, 0, 0.04);
    cv::cornerSubPix(I, interestPoints, subPixWinSize, cv::Size(-1,-1), termCriteria);


    resPoints.clear();
    for(auto p:interestPoints){
        double x=0,y=0;
        if(p.x>hw && p.x<c-hw && p.y>hw && p.y<r-hw){
            computePixel(p.y,p.x,x,y);
        }
        resPoints.push_back(cv::Point2f(p.x+x*10,p.y+y*10));
    }
}