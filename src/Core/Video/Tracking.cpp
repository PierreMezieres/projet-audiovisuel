//
// Created by pierre on 03/02/19.
//

#include <Log/Log.hpp>
#include "Tracking.hpp"
#include "Util.hpp"

Tracking::Tracking() {

}

SceneObjects Tracking::getSceneObjects() {
    return sceneObjects;
}

Objects Tracking::getObjects() {
    return objectsFinal;
}

void Tracking::updateTracking(Points p1_, Points p2_) {
    p1 = p1_;
    p2 = p2_;
    search_object();
    merge_object(objects);
    existing_object();
    new_object();
    merge_object(objectsFinal);
    updateSceneObjects();

}

void Tracking::existing_object() {
    std::vector<int> to_remove;
    std::vector<int> to_remove_bis;
    std::map<int, bool> used;
    bool corresp;
    for(auto o:objects) used[o.first] = false;
    for(auto o:objectsFinal){
        corresp = false;
        for(auto i:objects){
            if(used[i.first]==false){
                if(toMerge(o.second,i.second)){
                    if(corresp){
                        auto tmp = objectsFinal[o.first];
                        MergeRec(tmp, i.second);
                        objectsFinal[o.first] = tmp;
                    }else{
                        corresp = true;
                        objectsFinal[o.first] = i.second;
                    }

                    to_remove_bis.push_back(i.first);
                    used[i.first]=true;
                }
            }else{
                to_remove_bis.push_back(i.first);
            }
        }
        if(!corresp){
            o.second.warning++;
            if(o.second.warning>=m_persistence_object) to_remove.push_back(o.first);
            objectsFinal[o.first]=o.second;
        }
    }
    for(auto t:to_remove){
        objectsFinal.erase(t);
    }
    for(auto t:to_remove_bis){
        objects.erase(t);
    }
}

void Tracking::new_object() {
    for(auto o:objects){
        objectsFinal[area] = o.second;
        area++;
    }
}

void Tracking::merge_object(Objects &objects) {
    std::vector<int> follow;
    std::vector<int> to_remove;
    follow.resize(objects.size(),-1);
    //for(auto :objects) follow.push_back(-1);
    int i = 0;
    for(auto o1 = objects.begin(); o1 != objects.end(); o1++){
        if(follow[i]==-1){
            int j = 0;
            for(auto o2 = o1; o2 != objects.end(); o2++){
                if(o2 != o1){
                    if(toMerge(o1->second,o2->second)){
                        follow[i+j] = i;
                        MergeRec(o1->second,o2->second);
                    }

                }
                j++;
            }
        }else{
            to_remove.push_back(o1->first);
        }
        i++;
    }

    for(auto i:to_remove){
        objects.erase(i);
    }
}


void Tracking::search_object() {
    std::vector<int> follow;
    objects.clear();
    for (uint i = 0; i < p1.size(); i++) follow.push_back(i);
    for (uint i = 0; i < p1.size(); i++) {
        //if (follow[i] == -1) {
            Vector2 pt1(p1[i].x, p1[i].y);
            Vector2 dir1(p2[i].x - p1[i].x, p2[i].y - p1[i].y);
            float speed1 = glm::length(dir1);
            for (uint j = 0; j < p1.size(); j++) {
                if (follow[j] == (int)j) {
                    Vector2 pt2(p1[j].x, p1[j].y);
                    Vector2 dir2(p2[j].x - p1[j].x, p2[j].y - p1[j].y);
                    float speed2 = glm::length(dir2);
                    float dist = glm::distance(pt1, pt2);
                    float dot = glm::dot(glm::normalize(dir1), glm::normalize(dir2));
                    float speed_dif = std::abs(speed1 - speed2);

                    if (dot > m_dot_product && dist<m_distance && speed_dif < m_speed_dif) {
                        auto it = objects.find(follow[i]);
                        Object object;
                        if (it == objects.end()) {
                            object.id = follow[i];
                            object.top_left = pt1;
                            object.bot_right = pt1;
                        } else {
                            object = it->second;
                        }


                        object.top_left.x = std::min(object.top_left.x, pt2.x);
                        object.top_left.y = std::min(object.top_left.y, pt2.y);
                        object.bot_right.x = std::max(object.bot_right.x, pt2.x);
                        object.bot_right.y = std::max(object.bot_right.y, pt2.y);
                        object.number_vector++;

                        //follow[i] = i;
                        follow[j] = follow[i];

                        objects[follow[i]] = object;
                    }
                }
            }
        //}
    }
    std::vector<int> to_remove;
    for(auto i:objects){
        if(i.second.number_vector<m_minimum_vectors) to_remove.push_back(i.first);
    }
    for(auto i:to_remove){
        objects.erase(i);
    }
}

void Tracking::updateSceneObjects() {
    sceneObjects.clear();
    for(auto i:objectsFinal){
        SceneObject sceneObject;
        sceneObject.id = i.first;
        sceneObject.size = getSizeObject(i.second);
        sceneObject.position = getPositionObject(i.second);
        sceneObjects.push_back(sceneObject);
    }
}

void Tracking::cleanUp() {
    objects.clear();
    area = 0;
}

