//
// Created by pierre on 01/01/19.
//

#include <Log/Log.hpp>
#include "Deriv.hpp"

Deriv::Deriv(Mat i, Mat ip) {
    int r = i.rows;
    int c = i.cols;

    if(ip.rows!=r || ip.cols!=c){
        Log(logError) << "Construct deriv, not the same for mat";
        return;
    }

    I = i;
    Iprev = ip;
    computeDeriv();
}

void Deriv::computeDeriv() {
    int r = I.rows;
    int c = I.cols;


    Ix.create(r, c, CV_32S);
    Iy.create(r, c, CV_32S);
    It.create(r, c, CV_32S);
    Ix.setTo(0);
    Iy.setTo(0);
    It.setTo(0);


    for(int i=1; i<r-1; i++){
        for(int j=1; j<c-1; j++){
            Ix.at<int>(i,j) = I.at<uchar>(i,j+1) - I.at<uchar>(i,j-1);
            Iy.at<int>(i,j) = I.at<uchar>(i+1,j) - I.at<uchar>(i-1,j);
            It.at<int>(i,j) = I.at<uchar>(i,j) - Iprev.at<uchar>(i,j);
        }
    }
/*s
    for(int i=0; i<r; i++){
        Ix.at<int>(i,0) = I.at<int>(i,0) - I.at<int>(i,1);
        Ix.at<int>(i,c-1) = I.at<int>(i,c-1) - I.at<int>(i,c-2);

        if(i!=0 && i!=r-1){
            Iy.at<int>(i,0) = I.at<int>(i+1,0) - I.at<int>(i-1,0);
            Iy.at<int>(i,c-1) = I.at<int>(i+1,c-1) - I.at<int>(i-1,c-1);
        }

        It.at<int>(i,0) = I.at<int>(i,0) - Iprev.at<int>(i,0);
        It.at<int>(i,c-1) = I.at<int>(i,c-1) - Iprev.at<int>(i,c-1);
    }

    for(int j=0; j<c; j++){
        if(j!=0 && j!=c-1){
            Ix.at<int>(0,j) = I.at<int>(0,j+1) - I.at<int>(0,j-1);
            Ix.at<int>(r-1,j) = I.at<int>(r-1,j+1) - I.at<int>(r-1,j-1);
        }

        Iy.at<int>(0,j) = I.at<int>(0,j) - I.at<int>(1,j);
        Iy.at<int>(0,j) = I.at<int>(r-1,j) - I.at<int>(r-2,j);

        It.at<int>(0,j) = I.at<int>(0,j) - Iprev.at<int>(0,j);
        It.at<int>(r-1,j) = I.at<int>(r-1,j) - Iprev.at<int>(r-1,j);
    }
*/

}

Mat Deriv::getIt() {
    return It;
}

Mat Deriv::getIx() {
    return Ix;
}

Mat Deriv::getIy() {
    return Iy;
}