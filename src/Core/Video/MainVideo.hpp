//
// Created by pierre on 03/02/19.
//

#ifndef PROJECT_MAINVIDEO_HPP
#define PROJECT_MAINVIDEO_HPP


#include <Macros.hpp>
#include "LucasKanade.hpp"
#include "Tracking.hpp"

class MainVideo {
public:
    MainVideo();

    void update(Mat f1, Mat f2);

    void display();
    void displayDebug();

    SceneObjects getSceneObjects();

    void setMethodLK(int method);
    void setSpeedMin(int speed);

    void cleanUp();

    Tracking tracking;

private:

    Objects objects;
    Points p1,p2;

    int methodLK=0;
    int speedMin = 5;

    Mat frameColor,frame;

    LucasKanade lk = LucasKanade(Mat(), Mat(),0);



};


#endif //PROJECT_MAINVIDEO_HPP
