//
// Created by pierre on 02/01/19.
//

#include <Log/Log.hpp>
#include "Sound.hpp"

#include <math.h>

#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <algorithm>

Sound::Sound() {

    if(!initOpenAL()) {
        Log(logError) << "Error openAL";
        return;
    }

    // initialize random seed:
    srand(time(NULL));

    alGenSources(MAX_SOURCES, sources);

    alGenBuffers(NB_STEPS_BUFFERS, stepsBuffers);

    alGenSources(1, &userSource);

    alGenBuffers(1, &userBuffer);

    for(uint i = 0; i < NB_STEPS_BUFFERS; i++) {
        SF_INFO FileInfos;
        std::string filename = std::string("../../VS/singleSteps/step-") + std::to_string(i+1) + std::string(".wav");
        SNDFILE* File = sf_open(filename.c_str(), SFM_READ, &FileInfos);
        if (!File) {
            Log(logError) << "Error opening file " << filename;
            return;
        }

        // Lecture du nombre d'échantillons et du taux d'échantillonnage (nombre d'échantillons à lire par seconde)
        ALsizei NbSamples  = static_cast<ALsizei>(FileInfos.channels * FileInfos.frames);
        ALsizei SampleRate = static_cast<ALsizei>(FileInfos.samplerate);

        // Lecture des échantillons audio au format entier 16 bits signé (le plus commun)
        // range : −32,768 to 32,767
        std::vector<ALshort> Samples(static_cast<ulong>(NbSamples));
        if (sf_read_short(File, &Samples[0], NbSamples) < NbSamples) {
            Log(logError) << "Error loading sound from file";
            return;
        }

        // Fermeture du fichier
        sf_close(File);

        // Détermination du format en fonction du nombre de canaux
        ALenum Format;
        switch (FileInfos.channels)
        {
            case 1 :  Format = AL_FORMAT_MONO16;   break;
            case 2 :  Format = AL_FORMAT_STEREO16; break;
            default : Log(logError) << "incompatible file format"; return;
        }

        // Remplissage avec les échantillons lus
        alBufferData(stepsBuffers[i], Format, &Samples[0], NbSamples * sizeof(ALushort), SampleRate);

        // Vérification des erreurs
        if (alGetError() != AL_NO_ERROR) {
            Log(logError) << "AL error ";
            return;
        }
    }

    alGenBuffers(NB_SOUND_FILES, filesBuffers);

    //exemple
    //loadSoundFile(std::string("../../VS/singleSteps/fileName.wav"), enumName);
    //loadSoundFile(std::string("../../VS/sounds/demoAEnlever.wav"), demo);
    loadSoundFile("../../VS/sounds/pacman.wav", pacman);
    loadSoundFile("../../VS/sounds/mario_burned.wav",mario_burned);
    loadSoundFile("../../VS/sounds/mario_burned_short.wav",mario_burned_short);
    loadSoundFile("../../VS/sounds/mario_hoo.wav",mario_ooh);
    loadSoundFile("../../VS/sounds/mario_ah.wav",mario_ah);
    loadSoundFile("../../VS/sounds/saber.wav",saber);
    loadSoundFile("../../VS/sounds/weapon.wav",weapon);
    loadSoundFile("../../VS/sounds/pew.wav",pew);
    loadSoundFile("../../VS/sounds/link_jump.wav",link_jump);
    loadSoundFile("../../VS/sounds/link_hurt.wav",link_hurt);
    loadSoundFile("../../VS/sounds/quack.wav",quack);
    loadSoundFile("../../VS/sounds/kerboom.wav",kerboom);
    loadSoundFile("../../VS/sounds/coucou.wav",coucou);
}

Sound::~Sound() {
    shutdownOpenAL();
}

bool Sound::initOpenAL() {
    // Ouverture du device
    ALCdevice* Device = alcOpenDevice(NULL);
    //Log(logDebug) << Device;
    if (!Device)
        return false;

    // Création du contexte
    ALCcontext* Context = alcCreateContext(Device, NULL);
    if (!Context)
        return false;

    // Activation du contexte
    if (!alcMakeContextCurrent(Context))
        return false;


   // ALfloat ListenerPos[] = { 0.0, 0.0, 0.0 };
    //alListenerfv(AL_POSITION, ListenerPos);

    return true;
}

void Sound::shutdownOpenAL() {
    // Récupération du contexte et du device
    ALCcontext* Context = alcGetCurrentContext();
    ALCdevice*  Device  = alcGetContextsDevice(Context);

    // Désactivation du contexte
    alcMakeContextCurrent(NULL);

    // Destruction du contexte
    alcDestroyContext(Context);

    // Fermeture du device
    alcCloseDevice(Device);
}

void Sound::removeMissingObjects(SceneObjects frame) {
    bool found;
    for (auto it = objects.cbegin(); it != objects.cend(); ) {
        found = false;
        for(uint i = 0; i < frame.size(); i++) {
            if(it->first == frame[i].id) {
                found = true;
                ++it;
                break;
            }
        }
        if(!found)
            objects.erase(it++);
    }
}

std::vector<ALshort> Sound::genNote(float frequency) {
    float rate = 44100.f;
    float duration = 2.f;
    std::vector<ALshort> sound( static_cast<ulong>(rate * duration) );

    float radialisation = 2.f * M_PIf32 / rate;
    for(uint i = 0; i < sound.size(); i++) {
        sound[i] = static_cast<ALshort>(sin( i * radialisation * frequency) * 32767);
    }

    return sound;
}

std::vector<ALshort> Sound::genShepardNote(float frequency) {
    float rate = 44100.f;
    uint nbOcil = 10;
    float maxFreq = 20000.f;

    std::vector<ALshort> sound( static_cast<ulong>(rate * 0.1) );

    std::vector<float> freqs(nbOcil);
    freqs[0] = frequency;
    for(uint i = 1; i < freqs.size(); i++) {
        freqs[i] = freqs[i-1] / 2;
    }

    float rad = 2.f * M_PIf32 / rate;
    float sum;
    float toAdd;
    for(uint i = 0; i < sound.size(); i++) {
        sum = 0;
        for(uint j = 0; j < freqs.size(); j++) {
            toAdd = sin(i * rad * freqs[j]);

            //disparition graduelle des ocilateurs
            if(freqs[j] > maxFreq / 8) {
                toAdd *= 1 - (freqs[j] - (maxFreq / 8)) / (7 * maxFreq / 8);
            }
            sum += toAdd;
        }
        if(i < sound.size() * 0.1f) {
            sum *= i / sound.size() * 0.1f;
        } else if(i > sound.size() * 0.8f) {
            sum *= (i - sound.size() * 0.8f) / sound.size() * 0.2f;
        }
        sound[i] = static_cast<ALshort>(sum / nbOcil * 32767.f);
    }
    return sound;
}

std::vector<ALshort> Sound::genShepardScale(float duration, float lapDuration) {
    float rate = 44100.f;
    float minFreq = 50;
    float maxFreq = 3000;
    std::vector<ALshort> sound( static_cast<ulong>(rate * duration) );

    float rad = 2.f * M_PIf32 / rate;
    float step = (maxFreq - minFreq) / (lapDuration * rate);
    float freq = minFreq;
    float x = 0;
    for(uint i = 0; i < sound.size(); i++) {
        freq = freq + freq / 30000;
        //std::cout << freq << std::endl;
        if(freq > maxFreq)
            freq = minFreq;

        x += rad * freq;
        if(x > 2.f * M_PIf32)
            x -= 2.f * M_PIf32;

        sound[i] = static_cast<ALshort>(std::sin(x) * 32767.f);
    }
    //std::cout << rad << std::endl;

    return sound;
}

std::vector<ALshort> Sound::genShepardTone(float duration, float speed) {
    float rate = 44100.f;
    uint nbOcil = 10;
    float maxFreq = 20000.f;
    float minFreq = maxFreq / (pow(2, nbOcil));

    //Log(logDebug) << "minFreq : " << minFreq;
    std::vector<ALshort> sound( static_cast<ulong>(rate * duration) );

    std::vector<float> freqs(nbOcil);
    std::vector<float> travel(nbOcil);
    freqs[0] = maxFreq;
    for(uint i = 1; i < freqs.size(); i++) {
        freqs[i] = freqs[i-1] / 2;
        travel[i] = 0;
        //Log(logDebug) << "Freq " << i << " : " << freqs[i];
    }

    float rad = 2.f * M_PIf32 / rate;
    float sum;
    float toAdd;
    for(uint i = 0; i < sound.size(); i++) {
        sum = 0;
        for(uint j = 0; j < freqs.size(); j++) {
            if(freqs[j] >= maxFreq)
                freqs[j] = minFreq;
            freqs[j] += freqs[j] / speed;

            travel[j] += rad * freqs[j];
            if(travel[j] > 2.f * M_PIf32)
                travel[j] -= 2.f * M_PIf32;

            toAdd = sin(travel[j]);
            //disparition graduelle des ocilateurs

            if(freqs[j] > maxFreq / 8) {
                toAdd *= 1 - (freqs[j] - (maxFreq / 8)) / (7 * maxFreq / 8);
            }
            /*
            if(freqs[j] < maxFreq / 20) {
                toAdd *= freqs[j] / (1 * maxFreq / 20);
            }*/

            sum += toAdd;
        }
        sound[i] = static_cast<ALshort>(sum / nbOcil * 32767.f);
    }

    return sound;
}

void Sound::loadSoundFile(std::string filename, SoundFile enumFile) {
    SF_INFO FileInfos;
    SNDFILE* File = sf_open(filename.c_str(), SFM_READ, &FileInfos);
    if (!File) {
        Log(logError) << "Error opening file " << filename;
        return;
    }

    // Lecture du nombre d'échantillons et du taux d'échantillonnage (nombre d'échantillons à lire par seconde)
    ALsizei NbSamples  = static_cast<ALsizei>(FileInfos.channels * FileInfos.frames);
    ALsizei SampleRate = static_cast<ALsizei>(FileInfos.samplerate);

    // Lecture des échantillons audio au format entier 16 bits signé (le plus commun)
    // range : −32,768 to 32,767
    std::vector<ALshort> Samples(static_cast<ulong>(NbSamples));
    if (sf_read_short(File, &Samples[0], NbSamples) < NbSamples) {
        Log(logError) << "Error loading sound from file";
        return;
    }

    // Fermeture du fichier
    sf_close(File);

    // Détermination du format en fonction du nombre de canaux
    ALenum Format;
    switch (FileInfos.channels)
    {
        case 1 :  Format = AL_FORMAT_MONO16;   break;
        case 2 :  Format = AL_FORMAT_STEREO16; break;
        default : Log(logError) << "incompatible file format"; return;
    }

    // Remplissage avec les échantillons lus
    alBufferData(filesBuffers[enumFile], Format, &Samples[0], NbSamples * sizeof(ALushort), SampleRate);

    // Vérification des erreurs
    if (alGetError() != AL_NO_ERROR) {
        Log(logError) << "AL error ";
        return;
    }
}

std::vector<ALshort> Sound::genShepardTone2(float speed) {
    float rate = 44100.f;
    uint nbOcil = 10;
    float maxFreq = 20000.f;
    float minFreq = maxFreq / (pow(2, nbOcil));

    std::vector<ALshort> sound( static_cast<ulong>(rate / 1000.f * frameDuration) );

    std::vector<float> freqs(nbOcil);
    std::vector<float> travel(nbOcil);
    freqs[0] = currentFreq;
    for(uint i = 1; i < freqs.size(); i++) {
        freqs[i] = freqs[i-1] / 2;
        travel[i] = 0;
    }

    float rad = 2.f * M_PIf32 / rate;
    float sum;
    float toAdd;
    for(uint i = 0; i < sound.size(); i++) {
        sum = 0;
        for(uint j = 0; j < freqs.size(); j++) {
            if(freqs[j] >= maxFreq)
                freqs[j] = minFreq;
            freqs[j] += freqs[j] / speed;

            travel[j] += rad * freqs[j];
            if(travel[j] > 2.f * M_PIf32)
                travel[j] -= 2.f * M_PIf32;

            toAdd = sin(travel[j]);
            //disparition graduelle des ocilateurs

            if(freqs[j] > maxFreq / 8) {
                toAdd *= 1 - (freqs[j] - (maxFreq / 8)) / (7 * maxFreq / 8);
            }

            sum += toAdd;
        }
        sound[i] = static_cast<ALshort>(sum / nbOcil * 32767.f);
    }

    currentFreq = freqs[0];
    for(uint j = 1; j < freqs.size(); j++) {
        if(freqs[j] > currentFreq)
            currentFreq = freqs[j];
    }

    return sound;
}

void Sound::loadSound(std::string filename) {

    // Ouverture du fichier audio avec libsndfile
    SF_INFO FileInfos;
    SNDFILE* File = sf_open(filename.c_str(), SFM_READ, &FileInfos);
    if (!File)
        return;

    // Lecture du nombre d'échantillons et du taux d'échantillonnage (nombre d'échantillons à lire par seconde)
    ALsizei NbSamples  = static_cast<ALsizei>(FileInfos.channels * FileInfos.frames);
    ALsizei SampleRate = static_cast<ALsizei>(FileInfos.samplerate);

    // Lecture des échantillons audio au format entier 16 bits signé (le plus commun)
    // range : −32,768 to 32,767
    std::vector<ALshort> Samples(NbSamples);
    if (sf_read_short(File, &Samples[0], NbSamples) < NbSamples)
        return;

    // Fermeture du fichier
    sf_close(File);

    // Détermination du format en fonction du nombre de canaux
    ALenum Format;
    switch (FileInfos.channels)
    {
        case 1 :  Format = AL_FORMAT_MONO16;   break;
        case 2 :  Format = AL_FORMAT_STEREO16; break;
        default : return;
    }

    //--DEBUG--

    ALuint Buffer;
    alGenBuffers(1, &Buffer);

    //Samples = genNote(3000);
    //Samples = genShepardScale(8, 30000);
    Samples = genShepardTone(20, 2 * 318000);
    alBufferData(Buffer, AL_FORMAT_MONO16, &Samples[0], Samples.size() * sizeof(ALushort), 44100.f);

    SNDFILE* FileWrite = sf_open("testShep.wav", SFM_WRITE, &FileInfos);
    sf_write_short(FileWrite, &Samples[0], Samples.size());
    sf_close(FileWrite);

    //--DEBUG--END

    //Samples = genShepardScale(4, 1);
    //Samples = genShepardTone(60, 2 * 318000);

    // Remplissage avec les échantillons lus
    alBufferData(userBuffer, Format, &Samples[0], Samples.size() * sizeof(ALushort), SampleRate);

    // Vérification des erreurs
    if (alGetError() != AL_NO_ERROR)
        return;

    alSourcei(sources[0], AL_BUFFER, userBuffer);
}

void Sound::play() {
    int idStep = rand() % NB_STEPS_BUFFERS;
    ALuint source = getNextSource();
    alSourceStop(source);
    switch(soundToPlay) {
    case steps:
        alSourcei(source, AL_BUFFER, stepsBuffers[idStep]);
        break;
    default:
        alSourcei(source, AL_BUFFER, filesBuffers[soundToPlay]);
        break;

    }
    alSourcePlay(source);
}

void Sound::playShepard() {
    ALenum state;
    alGetSourcei(userSource, AL_SOURCE_STATE, &state);
    if(state == AL_PLAYING) {
        alSourceStop(userSource);
    } else {
        alSourcei(userSource, AL_BUFFER, userBuffer);
        alSourcePlay(userSource);
    }
}

void Sound::stop() {
    alSourceStop(sources[0]);
}

void Sound::nextFrame(SceneObjects frame) {
    //remove items which left the frame
    removeMissingObjects(frame);

    //add new objects and update old ones
    for(uint i = 0; i < frame.size(); i++) {
        auto it = objects.find(frame[i].id);
        if(it == objects.end()) {
            //new object : add it
            objects.insert( std::pair<ushort, objectInfo>(frame[i].id, {DISTANCE_TO_STEP, frame[i]}) );
        } else {
            //same object : update it
            Vector2 mvmt(it->second.sceneData.position.x - frame[i].position.x,
                         it->second.sceneData.position.y - frame[i].position.y);
            float distance = glm::length(mvmt);

            //Log(logDebug) << "object " << frame[i].id << " distance : " << distance;
            if(it->second.remainingDist > distance) {
                it->second.remainingDist -= distance;
            } else {
                it->second.remainingDist = DISTANCE_TO_STEP;

                float scaleFactor = 2 / (frame[i].size[0] / frameWidth + frame[i].size[1] / frameHeight);
                float angle = (1 - frame[i].position.x / frameWidth) * M_PIf32;
                ALfloat SourcePos[] = { cos(angle) * scaleFactor , 0.f, sin(angle) * scaleFactor };
                //Log(logDebug) << "source x : " << cos(angle) << " source y : " << sin(angle);
                alSourcefv(sources[currentSourceID], AL_POSITION, SourcePos);
                play();
            }
            it->second.sceneData = frame[i];
        }
    }
}

void Sound::nextFrameShepard(SceneObjects frame) {
    bool isNewFrame = false;
    float totalMovement = 0;

    //remove items which left the frame
    removeMissingObjects(frame);

    //add new objects and update old ones
    for(uint i = 0; i < frame.size(); i++) {
        auto it = objects.find(frame[i].id);
        if(it == objects.end()) {
            //new object : add it
            objects.insert( std::pair<ushort, objectInfo>(frame[i].id, {10000, frame[i]}) );
        } else {
            //same object : update it
            Vector2 mvmt(it->second.sceneData.position.x / frameWidth - frame[i].position.x / frameWidth,
                         it->second.sceneData.position.y / frameHeight - frame[i].position.y / frameHeight);
            float distance = glm::length(mvmt);
            totalMovement += distance;

            //si il a bougé
            if(distance > 0.00001f) {
                isNewFrame = true;
                float scaleFactor = 2 / (frame[i].size[0] / frameWidth + frame[i].size[1] / frameHeight);
                float angle = (1 - frame[i].position.x / frameWidth) * M_PIf32;
                ALfloat SourcePos[] = { cos(angle) * scaleFactor , 0.f, sin(angle) * scaleFactor };
                //Log(logDebug) << "source x : " << cos(angle) << " source y : " << sin(angle);
                alSourcefv(sources[currentSourceID], AL_POSITION, SourcePos);
                //play();
            }
            it->second.sceneData = frame[i];
        }
    }

    if(isNewFrame) {
        std::vector<ALshort> Samples;
        Samples = genShepardNote(currentFreq);
        if(currentFreq >= 20000.f)
            currentFreq /= 2.f;
        currentFreq += currentFreq/(200 - std::min(totalMovement * 400.f, 120.f));
        //Log(logDebug) << "totalmovement : " << totalMovement * 400.f;

        float scaleFactor = 2 / (frame[0].size[0] / frameWidth + frame[0].size[1] / frameHeight);
        float angle = (1 - frame[0].position.x / frameWidth) * M_PIf32;
        ALfloat SourcePos[] = { cos(angle) * scaleFactor , 0.f, sin(angle) * scaleFactor };
        //Log(logDebug) << "source x : " << cos(angle) << " source y : " << sin(angle);
        alSourcefv(userSource, AL_POSITION, SourcePos);

        alSourceStop(userSource);
        alSourcei(userSource, AL_BUFFER, 0);
        alDeleteBuffers(1, &userBuffer);
        alGenBuffers(1, &userBuffer);
        alBufferData(userBuffer, AL_FORMAT_MONO16, &Samples[0], Samples.size() * sizeof(ALushort), 44100.f);
        alSourcei(userSource, AL_BUFFER, userBuffer);
        alSourcePlay(userSource);
    }
}

/*
void Sound::nextFrameShepard(SceneObjects frame) {
    //remove items which left the frame
    removeMissingObjects(frame);

    if(frame.size() == 0)
        return;

    std::vector<ALshort> Samples(44100.f / 1000000.f * frameDuration);
    Samples = genShepardTone2(2 * 318000);
    alSourceStop(userSource);
    alSourcei(userSource, AL_BUFFER, 0);
    alDeleteBuffers(1, &userBuffer);
    alGenBuffers(1, &userBuffer);
    alBufferData(userBuffer, AL_FORMAT_MONO16, &Samples[0], Samples.size() * sizeof(ALushort), 44100.f);
    alSourcei(userSource, AL_BUFFER, userBuffer);
    alSourcePlay(userSource);

    //add new objects and update old ones
    for(uint i = 0; i < frame.size(); i++) {
        auto it = objects.find(frame[i].id);
        if(it == objects.end()) {
            //new object : add it
            objects.insert( std::pair<ushort, objectInfo>(frame[i].id, {DISTANCE_TO_STEP, frame[i]}) );
        } else {
            //same object : update it
            Vector2 mvmt(it->second.sceneData.position.x - frame[i].position.x,
                         it->second.sceneData.position.y - frame[i].position.y);
            float distance = glm::length(mvmt);

            //Log(logDebug) << "object " << frame[i].id << " distance : " << distance;
            if(it->second.remainingDist > distance) {
                it->second.remainingDist -= distance;
            } else {
                it->second.remainingDist = DISTANCE_TO_STEP;

                float scaleFactor = 2 / (frame[i].size[0] / frameWidth + frame[i].size[1] / frameHeight);
                float angle = (1 - frame[i].position.x / frameWidth) * M_PIf32;
                ALfloat SourcePos[] = { cos(angle) * scaleFactor , 0.f, sin(angle) * scaleFactor };
                //Log(logDebug) << "source x : " << cos(angle) << " source y : " << sin(angle);
                //alSourcefv(sources[currentSourceID], AL_POSITION, SourcePos);
                //play();
            }
            it->second.sceneData = frame[i];
        }
    }
}
*/

void Sound::updateFrameDim(int width, int height) {
    frameWidth = width;
    frameHeight = height;
    //Log(logDebug) << "width : " << width << " height : " << height;
}

//updates the duration of a frame (in ms)
void Sound::updateFrameDuration(float duration) {
    frameDuration = duration;
}

void Sound::setSoundFile(SoundFile file) {
    soundToPlay = file;
}

ALuint Sound::getNextSource() {
    ALuint currentSource = sources[currentSourceID];
    currentSourceID = (currentSourceID + 1) % MAX_SOURCES;
    return currentSource;
}
