//
// Created by pierre on 02/01/19.
//

#ifndef PROJECT_SOUND_HPP
#define PROJECT_SOUND_HPP

#define NB_STEPS_BUFFERS 16
#define MAX_SOURCES 5
#define DISTANCE_TO_STEP 160.f

#include <Macros.hpp>
#include <map>

struct objectInfo {
    float remainingDist;
    SceneObject sceneData;
};

#define NB_SOUND_FILES 14
enum SoundFile { steps = 0, pacman = 1, mario_burned = 2 , mario_burned_short=3, mario_ooh = 4, mario_ah = 5, saber = 6, weapon = 7, pew=8, link_jump=9, link_hurt=10, quack=11, kerboom = 12, coucou = 13};

class Sound {
public:
    Sound();
    ~Sound();

    void loadSound(std::string filename);
    void play();
    void playShepard();
    void stop();
    void nextFrame(SceneObjects frame);
    void nextFrameShepard(SceneObjects frame);
    void updateFrameDim(int width, int height);
    void updateFrameDuration(float duration);
    void setSoundFile(SoundFile file);

private:

    ALuint userSource;
    ALuint sources[MAX_SOURCES];
    ALuint userBuffer;
    ALuint stepsBuffers[NB_STEPS_BUFFERS];
    ALuint filesBuffers[NB_SOUND_FILES];
    std::map<ushort, objectInfo> objects;
    //SceneObjects prevFrame; //might be useless
    int frameWidth = 1, frameHeight = 1;
    float frameDuration = 100.f;

    SoundFile soundToPlay = steps;

    float currentFreq = 10000.f;
    std::vector<ALshort> genShepardTone2(float speed);

    uint currentSourceID = 0;
    ALuint getNextSource();

    bool initOpenAL();
    void shutdownOpenAL();

    void removeMissingObjects(SceneObjects frame);

    static std::vector<ALshort> genNote(float frequency);
    static std::vector<ALshort> genShepardNote(float frequency);
    static std::vector<ALshort> genShepardScale(float duration, float lapDuration);
    static std::vector<ALshort> genShepardTone(float duration, float speed);

    void loadSoundFile(std::string filename, SoundFile enumFile);
};


#endif //PROJECT_SOUND_HPP
