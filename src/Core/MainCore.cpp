//
// Created by pierre on 01/01/19.
//

#include <Log/Log.hpp>
#include <zconf.h>
#include "MainCore.hpp"

MainCore::MainCore(){

}

void MainCore::setVideo(std::string name){
    video = cv::VideoCapture(name);

    // Check if camera opened successfully
    if(!video.isOpened()){
        Log(logError) << "Can't read video: " << name;
        return;
    }


    totalFrames = video.get(cv::CAP_PROP_FRAME_COUNT);
}

void MainCore::useWebcam() {

    auto cap = cv::VideoCapture(0);

    // Check if camera opened successfully
    if(!cap.isOpened()){
        Log(logError) << "Error opening video stream or file";
        return;
    }

    Mat f;
    bool first = true;
    mainVideo.cleanUp();

    while(1){

        Mat frame;
        // Capture frame-by-frame
        cap >> f;
        cap >> frame;
        if(!first) {

            // If the frame is empty, break immediately
            if (frame.empty())
                break;

            mainVideo.update(frame,f);
            if(debugMode) mainVideo.displayDebug();
            else mainVideo.display();

            if(shepardSteps)
                sound.nextFrameShepard(mainVideo.getSceneObjects());
            else
                sound.nextFrame(mainVideo.getSceneObjects());

            // Press  ESC on keyboard to exit
            char c = (char) cv::waitKey(1);
            if (c == 27){
                cap.release();
                cv::destroyAllWindows();
                break;
            }
            usleep(100); // 0.1 ms
        } else {
            sound.updateFrameDim(f.cols, f.rows);
        }

        first = false;
    }
}

void MainCore::setFrame(int f) {
    frame=f;
}

bool MainCore::displayFrame(){

    if(!video.isOpened()){
        Log(logError) << "No video laoded";
        return false;
    }


    if(frame >= 0 && frame <= totalFrames){
        video.set(cv::CAP_PROP_POS_FRAMES,frame);
    }else{
        Log(logError) << "Frame: " << frame << " does not exist";
        return false;
    }

    Mat f;

    auto success = video.read(f);

    if(success)
        cv::imshow("Frame", f);
    return true;
}

void MainCore::playVideo(){
    double fps = 1000000/video.get(CV_CAP_PROP_FPS);
    Log(logInfo) << "w:"<<fps;
    Log(logInfo) << "You can press ESC on keyboard to stop video ! ";

    mainVideo.cleanUp();
    int f_save = frame;
    while(1){

        displayOpticalFlow();
        frame++;
        if(frame > totalFrames){
            frame = 1;
            return;
        }
        // Press  ESC on keyboard to exit
        char c=(char)cv::waitKey(1);
        if(c==27){
            frame = f_save;
            break;
        }
        usleep(fps); //TODO Improve by waiting fps minus execution time ...
    }
    //Log(logInfo) << "End video";

}

void MainCore::displayOpticalFlow() {
    if(!video.isOpened()){
        Log(logError) << "No video laoded";
        return;
    }


    if(frame >= 1 && frame <= totalFrames){
        video.set(cv::CAP_PROP_POS_FRAMES,frame-1);
    }else{
        Log(logError) << "Frame: " << frame << " does not exist";
        return;
    }



    Mat f1,f2;

    auto success = video.read(f1);
    auto success2 = video.read(f2);

    if(success && success2){
        mainVideo.update(f2,f1);
        if(debugMode) mainVideo.displayDebug();
        else mainVideo.display();

        sound.updateFrameDim(f1.cols, f2.rows);
        if(shepardSteps)
            sound.nextFrameShepard(mainVideo.getSceneObjects());
        else
            sound.nextFrame(mainVideo.getSceneObjects());
    }


}


void MainCore::debug() {
    if(!Vx.data){
        return;
    }

    cv::imshow("Debug Vx",Vx);
    cv::imshow("Debug Vy",Vy);

    for(int i=50;i<60;i++){
        std::string d;
        for(int j=50;j<60;j++){
            d =  d + std::to_string(Vx.at<double>(i,j)) + " ";
        }
        Log(logDebug) << d;
    }
}

void MainCore::setMethodLK(int i) {
    methodLK = i;
    mainVideo.setMethodLK(i);
}

void MainCore::toggleDebugMode() {
    debugMode = !debugMode;
}

void MainCore::toggleShepard(bool val) {
    shepardSteps = val;
}
