#include <QtWidgets/QFileDialog>
#include "MainWindow.hpp"
#include "ui_mainwindow.h"
#include <Macros.hpp>
#include <Log/Log.hpp>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    mainCore.sound.loadSound("../../VS/steps.wav");
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::on_actionVid_o_IRIT_triggered()
{
    mainCore.setVideo("../../VS/IRIT02.mpg");
    mainCore.displayFrame();
}

void MainWindow::on_spinBox_valueChanged(int arg1)
{
    mainCore.setFrame(arg1);
    mainCore.displayOpticalFlow();
}

void MainWindow::on_actionDebug_LucasKanade_triggered()
{
    mainCore.debug();
}

void MainWindow::on_comboBox_activated(int index)
{
    mainCore.setMethodLK(index);
}

void MainWindow::on_playButton_clicked()
{
    mainCore.playVideo();
    /*play = !play;
    if(!play){
        ui->playButton->setText("Play");
    }else{
        ui->playButton->setText("Pause");
    }*/
}

void MainWindow::on_checkBox_clicked()
{
    mainCore.toggleDebugMode();
}

void MainWindow::on_pushButton_clicked()
{
    mainCore.useWebcam();
}

void MainWindow::on_actionPendule_triggered()
{
    mainCore.setVideo("../../VS/penduleo.webm");

    mainCore.displayFrame();
}

void MainWindow::on_doubleSpinBox_2_valueChanged(double arg1)
{
    mainCore.mainVideo.tracking.m_dot_product=arg1;
}

void MainWindow::on_spinBox_2_valueChanged(int arg1)
{
    mainCore.mainVideo.tracking.m_distance=arg1;
}

void MainWindow::on_doubleSpinBox_valueChanged(double arg1)
{
    mainCore.mainVideo.tracking.m_speed_dif=arg1;
}

void MainWindow::on_spinBox_3_valueChanged(int arg1)
{
    mainCore.mainVideo.tracking.m_persistence_object=arg1;
}

void MainWindow::on_spinBox_4_valueChanged(int arg1)
{
    mainCore.mainVideo.tracking.m_minimum_vectors=arg1;
}

void MainWindow::on_checkBox_2_clicked(bool checked)
{
    mainCore.toggleShepard(checked);
}

void MainWindow::on_actionIRIT_02_triggered()
{
    mainCore.setVideo("../../VS/IRIT03.mpg");
    mainCore.displayFrame();
}

void MainWindow::on_actionIRIT_03_triggered()
{
    mainCore.setVideo("../../VS/IRIT04.mpg");
    mainCore.displayFrame();
}

void MainWindow::on_actionIRIT_04_triggered()
{
    mainCore.setVideo("../../VS/IRIT05.mpg");
    mainCore.displayFrame();
}

void MainWindow::on_spinBox_5_valueChanged(int arg1)
{
    mainCore.mainVideo.setSpeedMin(arg1);
}

void MainWindow::on_actionIRIT_05_triggered()
{
    mainCore.setVideo("../../VS/IRIT06.mpg");
    mainCore.displayFrame();
}

void MainWindow::on_comboBox_2_activated(int index)
{
    mainCore.sound.setSoundFile(SoundFile(index));
}
