#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <Core/MainCore.hpp>
#include <Core/Sound/Sound.hpp>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_comboBox_2_activated(int index);
    void on_actionIRIT_05_triggered();
    void on_spinBox_5_valueChanged(int arg1);
    void on_actionIRIT_04_triggered();
    void on_actionIRIT_03_triggered();
    void on_actionIRIT_02_triggered();
    void on_spinBox_4_valueChanged(int arg1);
    void on_spinBox_3_valueChanged(int arg1);
    void on_doubleSpinBox_valueChanged(double arg1);
    void on_spinBox_2_valueChanged(int arg1);
    void on_doubleSpinBox_2_valueChanged(double arg1);
    void on_actionPendule_triggered();
    void on_pushButton_clicked();
    void on_checkBox_clicked();
    void on_playButton_clicked();
    void on_comboBox_activated(int index);
    void on_actionDebug_LucasKanade_triggered();

    void on_spinBox_valueChanged(int arg1);

    void on_actionVid_o_IRIT_triggered();

    void on_checkBox_2_clicked(bool checked);

private:
    Ui::MainWindow *ui;

    MainCore mainCore;
    //Sound sound;

    bool play = false;
};

#endif // MAINWINDOW_HPP
