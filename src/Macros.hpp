//
// Created by pierre on 27/12/18.
//

#ifndef PROJECT_MACROS_HPP
#define PROJECT_MACROS_HPP

#include <vector>
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <glm/mat4x4.hpp>
#include <string>
#include <thread>


#include <opencv2/core/mat.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/videoio.hpp>

#include <AL/al.h>
#include <AL/alc.h>
#include <sndfile.h>
#include <map>

typedef glm::vec3 Vector3;
typedef glm::vec2 Vector2;
typedef glm::vec4 Vector4;
typedef glm::vec3 Point3;
typedef glm::vec2 Point2;
typedef glm::vec3 Normal;
typedef glm::uvec3 Triangle;
typedef glm::uvec2 Line;
typedef glm::vec3 Color;


typedef std::vector<Point3> VectorPoint3;
typedef std::vector<Point2> VectorPoint2;
typedef std::vector<Normal> VectorNormal;
typedef std::vector<Triangle> VectorTriangles;
typedef std::vector<double> VectorDouble;

typedef glm::mat4 Matrix4;
typedef glm::mat3 Matrix3;
typedef glm::mat2 Matrix2;

typedef cv::Mat Mat;


struct SceneObject {
    ushort id;
    Vector2 size;
    Vector3 position;
};

typedef struct Object{
    int id;
    Point2 top_left;
    Point2 bot_right;
    int warning = 0;
    int number_vector = 0;
}Object;

typedef std::vector<SceneObject> SceneObjects;
typedef std::map<int, Object> Objects;

typedef std::vector<cv::Point2f> Points;
typedef std::pair<Points,Points> Vectors;

#endif //PROJECT_MACROS_HPP
